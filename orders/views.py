from django.core.exceptions import ValidationError
from rest_framework import mixins as mx
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Address, Order
from .serializers import AddressSerializer, OrderSerializer


class AddressViewSet(viewsets.ModelViewSet):
	serializer_class = AddressSerializer
	queryset = Address.objects.all()

	def get_queryset(self):
		if self.request.user.is_superuser:
			return Address.objects.all()

		return Address.objects.filter(owner=self.request.user)

	def perform_create(self, serializer):
		user_addresses = Address.objects.filter(owner=self.request.user)

		if user_addresses.count() < 3:
			serializer.save(owner=self.request.user)
		else:
			raise ValidationError('user can have a maximum 3 addresses')

	@action(detail=True, methods=['get', ])
	def set_default_address(self, request, pk=None):
		address = self.get_object()
		address.is_default = True
		address.save()

		other_addresses = request.user.address_set.exclude(pk=address.id)
		if other_addresses:
			other_addresses.update(is_default=False)

		serializer = AddressSerializer(address)
		return Response(serializer.data, status=status.HTTP_200_OK)


class OrderViewSet(mx.CreateModelMixin, mx.ListModelMixin, mx.RetrieveModelMixin, viewsets.GenericViewSet):
	queryset = Order.objects.all()
	serializer_class = OrderSerializer

	def get_queryset(self):
		if self.request.user.is_superuser:
			return Order.objects.all()

		return Order.objects.filter(cart__owner=self.request.user)

	@action(detail=True, methods=['get', ])
	def cancel_order(self, request, pk=None):
		order = self.get_object()
		order.status = 'cancelled'
		order.save()

		serializer = OrderSerializer(order)
		return Response(serializer.data, status=status.HTTP_200_OK)

	@action(detail=False, methods=['get', ])
	def provider_orders(self, request):
		if request.user.is_provider:  # Confirm this check
			orders = Order.objects.filter(provider_emails__contains=request.user.email)
			serializer = OrderSerializer(orders, many=True)
			return Response(serializer.data, status=status.HTTP_200_OK)

		res = {'message': 'Insufficient permissions'}
		return Response(res, status=status.HTTP_403_FORBIDDEN)