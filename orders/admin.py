from django.contrib import admin

from .models import Address


class AddressAdmin(admin.ModelAdmin):
	list_display = ['pk', 'name', 'owner', 'address_type', 'is_default', 'updated']
	list_display_links =['name', ]


admin.site.register(Address, AddressAdmin)
