from rest_framework import serializers

from .models import Address, Order


class AddressSerializer(serializers.ModelSerializer):

	class Meta:
		model = Address
		exclude = ['owner', 'is_default']


class OrderSerializer(serializers.ModelSerializer):
	client_email = serializers.CharField()
	provider_emails = serializers.CharField()
	user_location = serializers.ListField(child=serializers.FloatField(), max_length=2, min_length=2)
	vat_amount = serializers.DecimalField(max_digits=10, decimal_places=2)
	grand_total = serializers.DecimalField(max_digits=10, decimal_places=2)

	class Meta:
		model = Order
		fields = '__all__'
