import random

from django.contrib.auth import get_user_model
from django.db import models


User = get_user_model()

ADDRESS_TYPES = (
	('shipping', 'Shipping'),
	('billing', 'Billing')
)

ORDER_STATUS = (
	('created', 'Created'),
	('paid', 'Paid'),
	('cancelled', 'Cancelled'),
	('dispatched', 'Dispatched'),
	('fulfilled', 'Fulfilled')
)

def get_unique_order_number():
	r = ''.join(str(random.randint(0,9)) for _ in range(7))
	qs = Order.objects.filter(order_no=r)
	if len(qs):
		get_unique_order_number()

	return r


class Address(models.Model):
	owner = models.ForeignKey(User, on_delete=models.CASCADE)

	name = models.CharField(max_length=99, null=True)
	address_type = models.CharField(max_length=8, choices=ADDRESS_TYPES)
	line_one = models.CharField(max_length=99)
	line_two = models.CharField(max_length=99, null=True)
	zip_code = models.CharField(max_length=5, null=True)
	town = models.CharField(max_length=99)
	state = models.CharField(max_length=99)
	country = models.CharField(max_length=99, default='Kenya')

	is_default = models.BooleanField(default=False)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):
		return '%s, %s - %s' %(self.line_one, self.town, self.country)


class Order(models.Model):
	cart = models.OneToOneField(Cart, on_delete=models.CASCADE)

	order_no = models.CharField(max_length=7, default=get_unique_order_number, db_index=True)
	status = models.CharField(max_length=10, default='created', choices=ORDER_STATUS)
	longitude = models.FloatField(null=True)
	latitude = models.FloatField(null=True)

	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):
		return 'Order No %s' % self.order_no

	@property
	def client_email(self):
		return self.cart.owner.email,

	@property
	def provider_emails(self):
		return self.cart.provider_emails

	@property
	def user_location(self):
		if self.longitude and self.latitude:
			return [self.longitude, self.latitude]

		return None

	@property
	def vat_amount(self):
		return self.cart.total * 0.16

	@property
	def grand_total(self):
		return self.cart.total * 1.16  # Plus any other charges
	