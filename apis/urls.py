from rest_framework import routers

from orders.views import AddressViewSet, OrderViewSet

router = routers.DefaultRouter()

router.register('addresses', AddressViewSet)
router.register('orders', OrderViewSet)

urlpatterns = router.urls
